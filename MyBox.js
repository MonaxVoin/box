

const dragElem = document.querySelector('.drag');//-ищем по имени класса
let offsetX = 0;//глобальные переменные
let offsetY = 0;




dragElem.addEventListener('mousedown', onMouseDown);//событие на элементе
document.addEventListener('mouseup', onMouseUp);//событие


const store = localStorage.getItem('inputValue');
const parseStore = JSON.parse(store)
// console.log(parseStore.value);


function onMouseDown(event) {// event  содержит инфу об объекте, нажатие
    offsetX = event.offsetX;//записываем новые значения по осям x and y
    offsetY = event.offsetY;
    document.addEventListener('mousemove', onMouseMove);
    dragElem.classList.add('move');//добавить класс
}


function onMouseMove(event) {//перемещение
    dragElem.style.left = event.pageX - offsetX + 'px';//получаем новые координаты перемещения
    dragElem.style.top = event.pageY - offsetY + 'px';//как в css

}


function onMouseUp(event) {//отпустил
    console.log(event);
    document.removeEventListener('mousemove', onMouseMove);//отписка от действия
    dragElem.classList.remove('move');//удалил класс
}



const wrapper = document.querySelector('.wrapper');//ищем теги по названию тегов
const modal = document.querySelector('.modal');
const button = document.querySelector('.close');
const inp = document.querySelector('input');




wrapper.addEventListener('click', closeModal);//подписываемся на событие
modal.addEventListener('click', stopProp);
button.addEventListener('click', saveToStorage);
inp.addEventListener('contextmenu', onContextMenu);
// inp.addEventListener('keydown', onKeyDown);




function closeModal() {
    wrapper.style.display = 'none';//закрытие окна 
}



function stopProp(event) {
    event.stopPropagation();//стоп распостронение
}



function onContextMenu(event) {
    console.log('Contex menu!');
    event.preventDefault(); //отключает стандартное поведение
}


// function onKeyDown(event) {
//     console.log(event);
//     if (event.key == 'g') {
//         event.preventDefault();
//         inp.value += 'a';//велью -что содержит инпут
//     }

// }


function saveToStorage() {
    const text = inp.value;
    const toStore = {
        value: text
    };
    // console.log(toStore);

    localStorage.setItem('inputValue', JSON.stringify(toStore));
    inp.value = '';
}
